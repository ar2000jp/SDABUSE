/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>

#ifdef GPIO_DEBUG
#define DEBUG 1
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "gpio.h"
#include "utility.h"

const struct _GPIO_PIN_VALUE GpioPinValue = {
    .LOW = 0,
    .HIGH = 1,
};

const struct _GPIO_PIN_MODE GpioPinMode = {
    .INPUT = 0,
    .OUTPUT = 1,
};

static const uint32_t GPIO_MEM_MAP_SIZE = 4096UL;
static const uint32_t GPIO_MEM_MAP_MASK = 4095UL; // (GPIO_MEM_MAP_SIZE - 1)

static uintptr_t GPIO_REG_BASE_ADDR;
static void* GPIO_MEM_MAP_BASE_PTR;
static int GPIO_MEM_FD;

static void* GPIO_PIN_MODE_PTR;
static void* GPIO_PIN_OUTPUT_PTR;
static void* GPIO_PIN_INPUT_PTR;

static const char GPIO_MEM_DEV_PATH[] = "/dev/mem";

void gpioSetBit(void* regPtr, uint8_t bit, uint8_t value);
uint8_t gpioGetBit(void* regPtr, uint8_t bit);

void gpioInit(uintptr_t gpioRegBaseAddress)
{
    if ((GPIO_MEM_FD = open(GPIO_MEM_DEV_PATH, O_RDWR | O_SYNC)) == -1) {
        LOG_FATAL("Failed to open memory access device %s", GPIO_MEM_DEV_PATH);
        exit(1);
    }
    LOG_DEBUG("Opened %s", GPIO_MEM_DEV_PATH);

    GPIO_REG_BASE_ADDR = gpioRegBaseAddress;

    /* Map one page */
    GPIO_MEM_MAP_BASE_PTR = mmap(0, GPIO_MEM_MAP_SIZE, PROT_READ | PROT_WRITE,
                                 MAP_SHARED, GPIO_MEM_FD,
                                 GPIO_REG_BASE_ADDR & ~GPIO_MEM_MAP_MASK);
    if (GPIO_MEM_MAP_BASE_PTR == (void*) MAP_FAILED) {
        LOG_FATAL("Failed to map memory.");
        exit(1);
    }
    LOG_DEBUG("Memory mapped at address %p.", GPIO_MEM_MAP_BASE_PTR);
}

void gpioDestroy(void)
{
    if (munmap(GPIO_MEM_MAP_BASE_PTR, GPIO_MEM_MAP_SIZE) == -1) {
        LOG_FATAL("Failed to unmap memory.");
        exit(1);
    };
    close(GPIO_MEM_FD);
}

void gpioSetModeRegOffset(uint8_t offset)
{
    GPIO_PIN_MODE_PTR = (void*)(((uint8_t*)GPIO_MEM_MAP_BASE_PTR) +
                                ((GPIO_REG_BASE_ADDR + offset) &
                                 GPIO_MEM_MAP_MASK));
}

void gpioSetInputRegOffset(uint8_t offset)
{
    GPIO_PIN_INPUT_PTR = (void*)(((uint8_t*)GPIO_MEM_MAP_BASE_PTR) +
                                 ((GPIO_REG_BASE_ADDR + offset) &
                                  GPIO_MEM_MAP_MASK));
}

void gpioSetOutputRegOffset(uint8_t offset)
{
    GPIO_PIN_OUTPUT_PTR = (void*)(((uint8_t*)GPIO_MEM_MAP_BASE_PTR) +
                                  ((GPIO_REG_BASE_ADDR + offset) &
                                   GPIO_MEM_MAP_MASK));
}

uint8_t gpioGetBit(void* regPtr, uint8_t bit)
{
    if (bit > 31) {
        LOG_ERROR("Bit index (%d) out of range.", bit);
        return 0;
    }

    uint32_t regValue;
    regValue = *((uint32_t*) regPtr);
    regValue = (regValue >> bit) & 1;
    return regValue;
}

void gpioSetBit(void* regPtr, uint8_t bit, uint8_t value)
{
    if (bit > 31) {
        LOG_ERROR("Bit index (%d) out of range.", bit);
        return;
    }
    if (value != 0) {
        value = 1;
    }

    uint32_t regValue;
    regValue = *((uint32_t*) regPtr);
    regValue = regValue & ~(1 << bit);      // Clear the bit
    regValue = regValue | (value << bit);   // Set the bit
    *((uint32_t*) regPtr) = regValue;
}

void gpioSetPinMode(uint8_t pin, uint8_t mode)
{
    gpioSetBit(GPIO_PIN_MODE_PTR, pin, mode);
}

uint8_t gpioGetPin(uint8_t pin)
{
    return gpioGetBit(GPIO_PIN_INPUT_PTR, pin);
}

void gpioSetPin(uint8_t pin, uint8_t value)
{
    gpioSetBit(GPIO_PIN_OUTPUT_PTR, pin, value);
}
