/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdint.h>
#include <stdbool.h>

#ifdef SD_DEBUG
#define DEBUG 1
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "sd.h"
#include "spi.h"
#include "utility.h"

const struct _SD_CMDS SdCommands = {
    .CMD0 = 0x00,
    .CMD8 = 0x08,
    .CMD9 = 0x09,
    .CMD10 = 0x0A,
    .CMD13 = 0x0D,
    .CMD17 = 0x11,
    .CMD24 = 0x18,
    .CMD25 = 0x19,
    .CMD32 = 0x20,
    .CMD33 = 0x21,
    .CMD38 = 0x26,
    .CMD55 = 0x37,
    .CMD58 = 0x3A,
    .ACMD23 = 0x17,
    .ACMD41 = 0x29,
};

const struct _SD_CARDTYPES SdCardTypes = {
    .UNKNOWN = 255,
    .SD1 = 1,
    .SD2 = 2,
    .SDHC = 3,
};

const struct _SD_STATES SdStates = {
    .R1_READY_STATE = 0x00,
    .R1_IDLE_STATE = 0x01,
    .R1_ILLEGAL_COMMAND = 0x04,
    .DATA_START_BLOCK = 0xFE,
    .STOP_TRAN_TOKEN = 0xFD,
    .WRITE_MULTIPLE_TOKEN = 0xFC,
    .DATA_RES_MASK = 0x1F,
    .DATA_RES_ACCEPTED = 0x05,
};

const struct _SD_ERRORS SdErrors = {
    .CMD0 = 0x1,
    .CMD8 = 0x2,
    .CMD17 = 0x3,
    .CMD24 = 0x4,
    .CMD25 = 0x05,
    .CMD58 = 0x06,
    .ACMD23 = 0x07,
    .ACMD41 = 0x08,
    .BAD_CSD = 0x09,
    .ERASE = 0x0A,
    .ERASE_SINGLE_BLOCK = 0x0B,
    .ERASE_TIMEOUT = 0x0C,
    .READ = 0x0D,
    .READ_REG = 0x0E,
    .READ_TIMEOUT = 0x0F,
    .STOP_TRAN = 0x10,
    .WRITE = 0x11,
    .WRITE_BLOCK_ZERO = 0x12,
    .WRITE_MULTIPLE = 0x13,
    .WRITE_PROGRAMMING = 0x14,
    .WRITE_TIMEOUT = 0x15,
    .SCK_RATE = 0x16,
};

const struct _SD_TIMEOUTS SdTimeouts = {
    .INIT_TIMEOUT = 2000,
    .ERASE_TIMEOUT = 10000,
    .READ_TIMEOUT = 300,
    .WRITE_TIMEOUT = 600,
};

struct CsdV1 {
    uint8_t csd_ver;
    uint8_t read_bl_len;
};

struct CsdV2 {
    uint8_t csd_ver;
    uint8_t read_bl_len;
    uint32_t c_size;
};

union Csd {
    struct CsdV1 v1;
    struct CsdV2 v2;
};

static uint8_t SD_TYPE;
static uint8_t SD_ERROR;
static uint16_t SD_BLOCKLEN;

uint8_t sdWaitStartBlock(void);
uint8_t sdWaitNotBusy(uint32_t timeoutMillis);
void sdFillCsd(uint8_t buf[], union Csd* csdptr);
uint16_t sdComputeCRC16(const uint8_t* data, const uint16_t length);

uint8_t sdCardAcmd(uint8_t cmd, uint32_t arg)
{
    sdCardCommand(SdCommands.CMD55, 0);
    return sdCardCommand(cmd, arg);
}

uint8_t sdWaitNotBusy(uint32_t timeoutMsecs)
{
    uint32_t t0 = getTimeOfDayMsecs();
    do {
        if (spiReadByte() == 0xFF) {
            return true;
        }
    } while ((getTimeOfDayMsecs() - t0) < timeoutMsecs);
    return false;
}

uint8_t sdCardCommand(uint8_t cmd, uint32_t arg)
{
    uint8_t status;

    // Select card
    spiSelectChip();

    // Wait up to 300 ms if busy
    sdWaitNotBusy(300);

    // Send command
    spiWriteByte(cmd | 0x40);

    // Send argument
    for (int8_t s = 24; s >= 0; s -= 8) {
        spiWriteByte(arg >> s);
    }

    // Send CRC
    //TODO: Implement full CRC
    uint8_t crc = 0xFF;
    if (cmd == SdCommands.CMD0) {
        crc = 0x95;    // Correct CRC for CMD0 with arg 0
    }
    if (cmd == SdCommands.CMD8) {
        crc = 0x87;    // Correct CRC for CMD8 with arg 0x1AA
    }
    spiWriteByte(crc);

    // Wait for response
    for (uint8_t i = 0; ((status = spiReadByte()) & 0x80) && i != 0xFF; i++);
    return status;
}

uint8_t sdReadCSD(union Csd* csdPtr)
{
    uint8_t bufArray[16];
    uint8_t retVal;

    retVal = sdReadRegister(SdCommands.CMD9, bufArray);
    sdFillCsd(bufArray, csdPtr);

    return retVal;
}

uint8_t sdReadRegister(uint8_t cmd, uint8_t* bufPtr)
{
    if (sdCardCommand(cmd, 0)) {
        LOG_ERROR("Card error: Register read failed.");
        goto fail;
    }
    if (!sdWaitStartBlock()) {
        goto fail;
    }
    // Transfer data
    spiRead(bufPtr, 16);

    spiReadByte(); // Get first CRC byte
    spiReadByte(); // Get second CRC byte
    spiDeselectChip();
    return true;

fail:
    spiDeselectChip();
    return false;
}

uint8_t sdWaitStartBlock(void)
{
    uint16_t t0 = getTimeOfDayMsecs();
    uint8_t status;
    while ((status = spiReadByte()) == 0xFF) {
        if (((uint16_t) getTimeOfDayMsecs() - t0) > SdTimeouts.READ_TIMEOUT) {
            LOG_ERROR("Card error: read timeout.");
            goto fail;
        }
    }
    if (status != SdStates.DATA_START_BLOCK) {
        SD_ERROR = SdErrors.READ;
        LOG_ERROR("Card error: error while reading data.");
        goto fail;
    }
    return true;

fail:
    spiDeselectChip();
    return false;
}

// Returns card size in bytes
uint64_t sdGetCardSize(void)
{
    union Csd csd;
    uint8_t retVal;

    retVal = sdReadCSD(&csd);
    if (!retVal) {
        return 0;
    }

    if (csd.v1.csd_ver == 0) {
//TODO: Implement me.
        LOG_ERROR("SD V1 CSD not implemented yet.");
    } else if (csd.v2.csd_ver == 1) {
        return (csd.v2.c_size + 1) << 19;
    } else {
        LOG_ERROR("Card error: Bad CSD.");
    }
    return 0;
}

// Returns card block size in bytes
uint16_t sdGetCardBlkSize(void)
{
    union Csd csd;
    uint8_t retVal;

    retVal = sdReadCSD(&csd);
    if (!retVal) {
        SD_BLOCKLEN = 0;
        return 0;
    }

    if (csd.v1.csd_ver == 0) {
        SD_BLOCKLEN = (1 << csd.v1.read_bl_len);
    } else if (csd.v2.csd_ver == 1) {
        SD_BLOCKLEN = (1 << csd.v2.read_bl_len);
    } else {
        LOG_ERROR("Card error: Bad CSD.");
        SD_BLOCKLEN = 0;
    }

    return SD_BLOCKLEN;
}

uint8_t sdInit(void)
{
    uint8_t status;
    uint32_t arg;
    uint32_t t0 = getTimeOfDayMsecs();
    SD_TYPE = 0;
    SD_ERROR = 0;

    // Must supply a minimum of 74 clock cycles with card deselected
    spiDeselectChip();
    for (uint8_t i = 0; i < 10; i++) {
        spiWriteByte(0xFF);
    }

    // Command to go idle in SPI mode
    spiSelectChip();
    while ((status = sdCardCommand(SdCommands.CMD0, 0)) !=
            SdStates.R1_IDLE_STATE) {
        if ((getTimeOfDayMsecs() - t0) > SdTimeouts.INIT_TIMEOUT) {
            SD_ERROR = SdErrors.CMD0;
            LOG_ERROR("Card error: CMD0 timeout.");
            goto fail;
        }
    }
    LOG_DEBUG("Card: CMD0 OK.");

    //TODO: CMD59 CRC_ON_OFF & implement CRC

    // Check SD version
    if ((sdCardCommand(SdCommands.CMD8, 0x1AA) & SdStates.R1_ILLEGAL_COMMAND)) {
        SD_TYPE = SdCardTypes.SD1;
        LOG_DEBUG("Card: CMD8 unsupported.");
    } else {
        // Only need last byte of R7 response
        for (uint8_t i = 0; i < 4; i++) {
            status = spiReadByte();
        }
        if (status != 0xAA) {
            SD_ERROR = SdErrors.CMD8;
            LOG_ERROR("Card error: CMD8 was not accepted.");
            goto fail;
        }
        SD_TYPE = SdCardTypes.SD2;
        LOG_DEBUG("Card: CMD8 OK.");
    }
    // Initialize card and send host supports SDHC if SD2
    arg = (SD_TYPE == SdCardTypes.SD2 ? 0x40000000 : 0);

    while ((status = sdCardAcmd(SdCommands.ACMD41, arg)) !=
            SdStates.R1_READY_STATE) {
        // Check for timeout
        if (((uint32_t) getTimeOfDayMsecs() - t0) > SdTimeouts.INIT_TIMEOUT) {
            SD_ERROR = SdErrors.ACMD41;
            LOG_ERROR("Card error: ACMD41 initialization process timeout.");
            goto fail;
        }
    }
    LOG_DEBUG("Card: ACMD41 OK.");

    // If SD2, read OCR register to check for SDHC card
    if (SD_TYPE == SdCardTypes.SD2) {
        if (sdCardCommand(SdCommands.CMD58, 0)) {
            SD_ERROR = SdErrors.CMD58;
            LOG_ERROR("Card error: error while reading OCR (CMD58).");
            goto fail;
        }
        if ((spiReadByte() & 0xC0) == 0xC0) {
            SD_TYPE = SdCardTypes.SDHC;
        }
        // Discard rest of OCR
        for (uint8_t i = 0; i < 3; i++) {
            spiReadByte();
        }
    }
    spiDeselectChip();
    return true;

fail:
    spiDeselectChip();
    SD_TYPE = SdCardTypes.UNKNOWN;
    return false;
}

void sdDestroy(void)
{
    // Command to go idle in SPI mode
    uint8_t status;
    uint32_t t0 = getTimeOfDayMsecs();

    spiSelectChip();
    while ((status = sdCardCommand(SdCommands.CMD0, 0))
            != SdStates.R1_IDLE_STATE) {
        if ((getTimeOfDayMsecs() - t0) > SdTimeouts.INIT_TIMEOUT) {
            SD_ERROR = SdErrors.CMD0;
            LOG_ERROR("Card error: CMD0 timeout.");
            goto fail;
        }
    }
    LOG_DEBUG("Card: CMD0 OK.");

fail:
    spiDeselectChip();
}

void sdEnableCrc(bool enable)
{
    //TODO: Implement me
    LOG_WARN("CRC not implemented yet.");
}

uint8_t sdGetCardType()
{
    return SD_TYPE;
}

uint8_t sdReadMultiBlock(uint32_t block, uint16_t count, uint8_t* bufPtr)
{
    //TODO: Use multi block read command
    // Use address if not SDHC card
    if (SD_TYPE != SdCardTypes.SDHC) {
        block <<= 9;
    }

    for (uint32_t i = 0; i < count; i++) {
        if (sdCardCommand(SdCommands.CMD17, block + i)) {
            SD_ERROR = SdErrors.CMD17;
            goto fail;
        }
        if (!sdWaitStartBlock()) {
            goto fail;
        }

        spiRead(&bufPtr[i * SD_BLOCKLEN], SD_BLOCKLEN);
    }
    return true;

fail:
    spiDeselectChip();
    return false;
}

// Send one block of data for write block or write multiple blocks
uint8_t sdWriteData(uint8_t token, const uint8_t* src)
{
    uint16_t crc;
    crc = sdComputeCRC16(src, SD_BLOCKLEN);

    spiWriteByte(token);
    spiWrite(src, SD_BLOCKLEN);

    spiWriteByte(crc >> 8);
    spiWriteByte(crc);

    uint8_t status_ = spiReadByte();
    if ((status_ & SdStates.DATA_RES_MASK) != SdStates.DATA_RES_ACCEPTED) {
        SD_ERROR = SdErrors.WRITE;
        spiDeselectChip();
        return false;
    }
    return true;
}

uint8_t sdWriteMultiBlock(uint32_t block, uint16_t count, const uint8_t* bufPtr)
{
    //TODO: Use multi block write command
    // Use address if not SDHC card
    if (SD_TYPE != SdCardTypes.SDHC) {
        block <<= 9;
    }
    for (uint32_t i = 0; i < count; i++) {
        if (sdCardCommand(SdCommands.CMD24, block + i)) {
            SD_ERROR = SdErrors.CMD24;
            goto fail;
        }
        if (!sdWriteData(SdStates.DATA_START_BLOCK,
                         bufPtr + (SD_BLOCKLEN * i))) {
            goto fail;
        }

        // Wait for flash programming to complete
        if (!sdWaitNotBusy(SdTimeouts.WRITE_TIMEOUT)) {
            SD_ERROR = SdErrors.WRITE_TIMEOUT;
            goto fail;
        }

        // Response is R2, so get and check two bytes for nonzero
        if (sdCardCommand(SdCommands.CMD13, 0) || spiReadByte()) {
            SD_ERROR = SdErrors.WRITE_PROGRAMMING;
            goto fail;
        }
        spiDeselectChip();
    }
    return true;

fail:
    spiDeselectChip();
    return false;
}

void sdFillCsdV2(uint8_t* bufPtr, struct CsdV2* csdV2Ptr)
{
    //TODO: Implement the rest
    csdV2Ptr->csd_ver = (bufPtr[0] & 0xC0) >> 6;

    csdV2Ptr->c_size = (bufPtr[7] & 0x3F) << 16;
    csdV2Ptr->c_size |= (bufPtr[8] << 8);
    csdV2Ptr->c_size |= (bufPtr[9]);

    csdV2Ptr->read_bl_len = (bufPtr[5] & 0x0F);
}

void sdFillCsd(uint8_t* bufPtr, union Csd* csdPtr)
{
    //TODO: Implement V1
    if (SD_TYPE == SdCardTypes.SD1) {
        LOG_ERROR("Unsupported card type.");
    } else if (SD_TYPE == SdCardTypes.SD2 ||
               SD_TYPE == SdCardTypes.SDHC) {
        sdFillCsdV2(bufPtr, &(csdPtr->v2));
    } else {
        LOG_ERROR("Unsupported card type.");
    }
}

uint16_t sdComputeCRC16(const uint8_t* data, const uint16_t length)
{
    //TODO: Implement me
    return 0xFFFF;
}
