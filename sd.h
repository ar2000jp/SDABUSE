/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SD_H
#define SD_H

// SD card commands
struct _SD_CMDS {
    /** GO_IDLE_STATE - init card in spi mode if CS low */
    const uint8_t CMD0;
    /** SEND_IF_COND - verify SD Memory Card interface operating condition.*/
    const uint8_t CMD8;
    /** SEND_CSD - read the Card Specific Data (CSD register) */
    const uint8_t CMD9;
    /** SEND_CID - read the card identification information (CID register) */
    const uint8_t CMD10;
    /** SEND_STATUS - read the card status register */
    const uint8_t CMD13;
    /** READ_BLOCK - read a single data block from the card */
    const uint8_t CMD17;
    /** WRITE_BLOCK - write a single data block to the card */
    const uint8_t CMD24;
    /** WRITE_MULTIPLE_BLOCK - write blocks of data until a STOP_TRANSMISSION */
    const uint8_t CMD25;
    /** ERASE_WR_BLK_START - sets the address of the first block to be erased */
    const uint8_t CMD32;
    /** ERASE_WR_BLK_END - sets the address of the last block of the continuous
        range to be erased*/
    const uint8_t CMD33;
    /** ERASE - erase all previously selected blocks */
    const uint8_t CMD38;
    /** APP_CMD - escape for application specific command */
    const uint8_t CMD55;
    /** READ_OCR - read the OCR register of a card */
    const uint8_t CMD58;
    /** SET_WR_BLK_ERASE_COUNT - Set the number of write blocks to be
         pre-erased before writing */
    const uint8_t ACMD23;
    /** SD_SEND_OP_COMD - Sends host capacity support information and
        activates the card's initialization process */
    const uint8_t ACMD41;
};

// SD card states
struct _SD_STATES {
    /** status for card in the ready state */
    const uint8_t R1_READY_STATE;
    /** status for card in the idle state */
    const uint8_t R1_IDLE_STATE;
    /** status bit for illegal command */
    const uint8_t R1_ILLEGAL_COMMAND;
    /** start data token for read or write single block*/
    const uint8_t DATA_START_BLOCK;
    /** stop token for write multiple blocks*/
    const uint8_t STOP_TRAN_TOKEN;
    /** start data token for write multiple blocks*/
    const uint8_t WRITE_MULTIPLE_TOKEN;
    /** mask for data response tokens after a write block operation */
    const uint8_t DATA_RES_MASK;
    /** write data accepted token */
    const uint8_t DATA_RES_ACCEPTED;
};

// SD card timeouts
struct _SD_TIMEOUTS {
    /** init timeout ms */
    const uint16_t INIT_TIMEOUT;
    /** erase timeout ms */
    const uint16_t ERASE_TIMEOUT;
    /** read timeout ms */
    const uint16_t READ_TIMEOUT;
    /** write time out ms */
    const uint16_t WRITE_TIMEOUT;
};

// SD card errors
struct _SD_ERRORS {
    /** timeout error for command CMD0 */
    const uint8_t CMD0;
    /** CMD8 was not accepted - not a valid SD card*/
    const uint8_t CMD8;
    /** card returned an error response for CMD17 (read block) */
    const uint8_t CMD17;
    /** card returned an error response for CMD24 (write block) */
    const uint8_t CMD24;
    /**  WRITE_MULTIPLE_BLOCKS command failed */
    const uint8_t CMD25;
    /** card returned an error response for CMD58 (read OCR) */
    const uint8_t CMD58;
    /** SET_WR_BLK_ERASE_COUNT failed */
    const uint8_t ACMD23;
    /** card's ACMD41 initialization process timeout */
    const uint8_t ACMD41;
    /** card returned a bad CSR version field */
    const uint8_t BAD_CSD;
    /** erase block group command failed */
    const uint8_t ERASE;
    /** card not capable of single block erase */
    const uint8_t ERASE_SINGLE_BLOCK;
    /** Erase sequence timed out */
    const uint8_t ERASE_TIMEOUT;
    /** card returned an error token instead of read data */
    const uint8_t READ;
    /** read CID or CSD failed */
    const uint8_t READ_REG;
    /** timeout while waiting for start of read data */
    const uint8_t READ_TIMEOUT;
    /** card did not accept STOP_TRAN_TOKEN */
    const uint8_t STOP_TRAN;
    /** card returned an error token as a response to a write operation */
    const uint8_t WRITE;
    /** attempt to write protected block zero */
    const uint8_t WRITE_BLOCK_ZERO;
    /** card did not go ready for a multiple block write */
    const uint8_t WRITE_MULTIPLE;
    /** card returned an error to a CMD13 status check after a write */
    const uint8_t WRITE_PROGRAMMING;
    /** timeout occurred during write programming */
    const uint8_t WRITE_TIMEOUT;
    /** incorrect rate selected */
    const uint8_t SCK_RATE;
};

// SD card types
struct _SD_CARDTYPES {
    /** Standard capacity V1 SD card */
    const uint8_t SD1;
    /** Standard capacity V2 SD card */
    const uint8_t SD2;
    /** High Capacity SD card */
    const uint8_t SDHC;

    const uint8_t UNKNOWN;
};

extern const struct _SD_CMDS SdCommands;
extern const struct _SD_STATES SdStates;
extern const struct _SD_TIMEOUTS SdTimeouts;
extern const struct _SD_ERRORS SdErrors;
extern const struct _SD_CARDTYPES SdCardTypes;

uint8_t sdInit(void);
void sdDestroy(void);

void sdEnableCrc(bool enable);

uint8_t sdGetCardType(void);
uint64_t sdGetCardSize(void);
uint16_t sdGetCardBlkSize(void);

uint8_t sdCardCommand(uint8_t cmd, uint32_t arg);
uint8_t sdCardAcmd(uint8_t cmd, uint32_t arg);
uint8_t sdReadRegister(uint8_t cmd, uint8_t* bufPtr);

uint8_t sdReadMultiBlock(uint32_t block, uint16_t count, uint8_t* bufPtr);
uint8_t sdWriteMultiBlock(uint32_t block, uint16_t count, const uint8_t* bufPtr);

#endif // SD_H
