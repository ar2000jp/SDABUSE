# SDABUSE
SD As a Block device in USErspace.

SDABUSE is an SD card driver in user space. It allows you to access an SD card connected to your system over SPI as an NBD block device. Currently, it supports accessing SPI over GPIO and SPIDev.

A read/write speed of \~340/\~100 KiB/s was achieved on a TL-WR741NDv1 running an OpenWRT build of the lede-17.01 branch.

SD code is based on the Arduino SD library. [BUSE](https://github.com/acozzette/BUSE) is used for the NBD support.

### License

Copyright 2018 Ahmad Draidi and the SDABUSE contributors.  
SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
