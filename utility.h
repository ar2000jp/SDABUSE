/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>

#define LOG_PRINT(...) do { fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); } while(0)

#define LOG_FATAL(...) do { fprintf(stderr, "FATAL: %s:%s:%d: (%d) [%s] ", \
                                        __FILE__, __func__, __LINE__, \
                                        errno, strerror(errno)); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); } while(0)

#define LOG_ERROR(...) do { fprintf(stderr, "ERROR: %s:%s:%d: ", \
                                        __FILE__, __func__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); } while(0)

#define LOG_WARN(...) do { fprintf(stderr, "WARN: %s:%s:%d: ", \
                                       __FILE__, __func__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); } while(0)

#define LOG_INFO(...) do { fprintf(stderr, "INFO: %s:%s:%d: ", \
                                       __FILE__, __func__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); } while(0)

#if DEBUG
#define LOG_DEBUG(...) do { fprintf(stderr, "DEBUG: %s:%s:%d: ", \
                                        __FILE__, __func__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); } while(0)
#else
#define LOG_DEBUG(...) do {} while(0)
#endif // DEBUG

uint32_t getTimeOfDayMsecs(void);

#endif // UTILITY_H
