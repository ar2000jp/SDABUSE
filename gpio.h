/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef GPIO_H
#define GPIO_H

#include <stdbool.h>
#include <stdint.h>

struct _GPIO_PIN_VALUE {
    const uint8_t LOW;
    const uint8_t HIGH;
};

struct _GPIO_PIN_MODE {
    const uint8_t OUTPUT;
    const uint8_t INPUT;
};

extern const struct _GPIO_PIN_VALUE GpioPinValue;
extern const struct _GPIO_PIN_MODE GpioPinMode;

void gpioInit(uintptr_t gpioRegBaseAddress);
void gpioDestroy(void);

void gpioSetModeRegOffset(uint8_t offset);
void gpioSetOutputRegOffset(uint8_t offset);
void gpioSetInputRegOffset(uint8_t offset);

void gpioSetPinMode(uint8_t pin, uint8_t mode);

void gpioSetPin(uint8_t pin, uint8_t value);
uint8_t gpioGetPin(uint8_t pin);

#endif // GPIO_H
