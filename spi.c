/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#ifdef SPI_DEBUG
#define DEBUG 1
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "spi.h"
#include "utility.h"
#include "gpio.h"

const struct _SPI_MODES SpiModes = {
    .DEV = 1,
    .GPIO = 2,
};

static uint8_t SPI_WRITE_MODE;
static uint8_t SPI_READ_MODE;

static char SPI_DEV_FILE_PATH[32];
static int SPI_DEV_FD;

static uint8_t SPI_SCK_PIN;
static uint8_t SPI_MISO_PIN;
static uint8_t SPI_MOSI_PIN;
static uint8_t SPI_CS_PIN;

static bool SPI_CS_ACTIVELOW;
static bool SPI_CS_GPIO;

static uint8_t* SPI_ONES_BUF_PTR;
static uint16_t SPI_ONES_BUF_SIZE;

uint16_t spiDevRead(uint8_t* dataBuf, uint16_t length);
uint16_t spiDevWrite(const uint8_t* dataBuf, uint16_t length);

uint16_t spiGpioRead(uint8_t* dataBuf, uint16_t length);
uint16_t spiGpioWrite(const uint8_t* dataBuf, uint16_t length);

uint8_t spiGpioReadByte(void);
void spiGpioWriteByte(uint8_t data);

uint8_t spiDevReadByte(void);
void spiDevWriteByte(uint8_t data);

void spiInit(void)
{
    if ((SPI_READ_MODE == SpiModes.DEV) || (SPI_WRITE_MODE == SpiModes.DEV)) {
        uint8_t  lsb, bits;
        uint32_t speed, mode;

        if ((SPI_DEV_FD = open(SPI_DEV_FILE_PATH, O_RDWR)) < 0) {
            LOG_FATAL("Failed to open SPIDev %s", SPI_DEV_FILE_PATH);
            exit(1);
        }

        if (ioctl(SPI_DEV_FD, SPI_IOC_RD_MODE32, &mode) < 0) {
            LOG_FATAL("Failed to read mode.");
            exit(1);
        }

        if (ioctl(SPI_DEV_FD, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
            LOG_FATAL("Failed to read LSB mode.");
            exit(1);
        }
        if (ioctl(SPI_DEV_FD, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) {
            LOG_FATAL("Failed to read word size.");
            exit(1);
        }
        if (ioctl(SPI_DEV_FD, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
            LOG_FATAL("Failed to read max speed.");
            exit(1);
        }

        LOG_INFO("SPI mode %u, %u bits per word, %s first, %u Hz max.",
                 mode & (SPI_CPOL | SPI_CPHA), bits, lsb ? "LSB" : "MSB", speed);
    }

    if ((SPI_READ_MODE == SpiModes.GPIO) || (SPI_WRITE_MODE == SpiModes.GPIO)) {
        gpioSetPinMode(SPI_CS_PIN, GpioPinMode.OUTPUT);
        gpioSetPinMode(SPI_SCK_PIN, GpioPinMode.OUTPUT);
        gpioSetPinMode(SPI_MOSI_PIN, GpioPinMode.OUTPUT);
        gpioSetPinMode(SPI_MISO_PIN, GpioPinMode.INPUT);
    }
}

void spiDestroy(void)
{
    if ((SPI_READ_MODE == SpiModes.DEV) || (SPI_WRITE_MODE == SpiModes.DEV)) {
        close(SPI_DEV_FD);
    }
}

void spiSetGpioPins(uint8_t csPin, uint8_t sckPin, uint8_t misoPin, uint8_t mosiPin)
{
    SPI_CS_PIN = csPin;
    SPI_SCK_PIN = sckPin;
    SPI_MISO_PIN = misoPin;
    SPI_MOSI_PIN = mosiPin;
}

void spiSetSpiDevPath(char* devFilePath)
{
    strncpy(SPI_DEV_FILE_PATH, devFilePath, sizeof(SPI_DEV_FILE_PATH));
}

void spiSetReadMode(uint8_t mode)
{
    SPI_READ_MODE = mode;
}

void spiSetWriteMode(uint8_t mode)
{
    SPI_WRITE_MODE = mode;
}

uint16_t spiRead(uint8_t* dataBuf, uint16_t length)
{
    LOG_DEBUG("Length: %u ", length);
    if (SPI_READ_MODE == SpiModes.DEV) {
        return spiDevRead(dataBuf, length);
    } else if (SPI_READ_MODE == SpiModes.GPIO) {
        return spiGpioRead(dataBuf, length);
    } else {
        LOG_FATAL("Invalid mode.");
        exit(1);
    }

    return 0;//FIXME: like spiWrite below
}

uint16_t spiWrite(const uint8_t* dataBuf, uint16_t length)
{
    LOG_DEBUG("Length: %u ", length);
    if (SPI_WRITE_MODE == SpiModes.DEV) {
        return spiDevWrite(dataBuf, length);
    } else if (SPI_WRITE_MODE == SpiModes.GPIO) {
        return spiGpioWrite(dataBuf, length);
    } else {
        LOG_FATAL("Invalid mode.");
        exit(1);
    }
    return 0;//FIXME: find ret meanings for ioctl. Probably should return number of bytes transferred
}

uint8_t spiReadByte(void)
{
    uint8_t data;
    spiRead(&data, 1);
    LOG_DEBUG("Data: 0x%X", data);
    return data;
}

void spiWriteByte(uint8_t data)
{
    LOG_DEBUG("Data: 0x%X", data);
    spiWrite(&data, 1);
}

uint16_t spiGpioRead(uint8_t* dataBuf, uint16_t length)
{
    LOG_DEBUG("Length: %u ", length);
    for (uint16_t i = 0; i < length; i++) {
        dataBuf[i] = spiGpioReadByte();
    }
    return 0;//TODO: like spiWrite
}

uint16_t spiGpioWrite(const uint8_t* dataBuf, uint16_t length)
{
    LOG_DEBUG("Length: %u ", length);
    for (uint16_t i = 0; i < length; i++) {
        spiGpioWriteByte(dataBuf[i]);
    }
    return 0;//TODO: like spiWrite
}

uint8_t spiGpioReadByte(void)
{
    uint8_t data = 0;

    // Set output pin high - like sending 0xFF
    gpioSetPin(SPI_MOSI_PIN, GpioPinValue.HIGH);

    for (uint8_t i = 0; i < 8; i++) {
        gpioSetPin(SPI_SCK_PIN, GpioPinValue.HIGH);

        data <<= 1;
        if (gpioGetPin(SPI_MISO_PIN)) {
            data |= 1;
        }
        gpioSetPin(SPI_SCK_PIN, GpioPinValue.LOW);
    }

    LOG_DEBUG("Data: 0x%X", data);
    return data;
}

void spiGpioWriteByte(uint8_t data)
{
    LOG_DEBUG("Data: 0x%X", data);
    for (uint8_t i = 0; i < 8; i++) {
        gpioSetPin(SPI_SCK_PIN, GpioPinValue.LOW);
        gpioSetPin(SPI_MOSI_PIN, data & 0x80);
        data <<= 1;
        gpioSetPin(SPI_SCK_PIN, GpioPinValue.HIGH);
    }

    gpioSetPin(SPI_SCK_PIN, GpioPinValue.LOW);
}

uint16_t spiDevRead(uint8_t* dataBuf, uint16_t length)
{
    if (length > SPI_ONES_BUF_SIZE) {
        if (SPI_ONES_BUF_SIZE > 0) {
            free(SPI_ONES_BUF_PTR);
        }
        SPI_ONES_BUF_SIZE = length;
        SPI_ONES_BUF_PTR = malloc(SPI_ONES_BUF_SIZE);
        memset(SPI_ONES_BUF_PTR, 0xFF, SPI_ONES_BUF_SIZE);
    }

    struct spi_ioc_transfer xfer = {
        .tx_buf = (uintptr_t) SPI_ONES_BUF_PTR,
        .rx_buf = (uintptr_t) dataBuf,
        .len = length,
    };

    int32_t status;
    status = ioctl(SPI_DEV_FD, SPI_IOC_MESSAGE(1), &xfer);
    if (status < 1) {
        LOG_FATAL("Failed to read.");
        LOG_DEBUG("Status: %d, Length: %u", status, length);
        exit(1);
    }
    LOG_DEBUG("Status: %d, Length: %u", status, length);
    return status;
}

uint16_t spiDevWrite(const uint8_t* dataBuf, uint16_t length)
{
    struct spi_ioc_transfer xfer = {
        .tx_buf = (uintptr_t) dataBuf,
        .rx_buf = (uintptr_t) NULL,
        .len = length,
    };

    int32_t status;
    if ((status = ioctl(SPI_DEV_FD, SPI_IOC_MESSAGE(1), &xfer)) < 1) {
        LOG_FATAL("Failed to write.");
        LOG_DEBUG("Status: %d, Length: %u", status, length);
        exit(1);
    }
    LOG_DEBUG("Status: %d, Length: %u", status, length);
    return status;
}

void spiSelectChip(void)
{
    if (SPI_CS_GPIO) {
        gpioSetPin(SPI_CS_PIN, !SPI_CS_ACTIVELOW);
    }
}

void spiDeselectChip(void)
{
    if (SPI_CS_GPIO) {
        gpioSetPin(SPI_CS_PIN, SPI_CS_ACTIVELOW);
    }
}

void spiSetCsActiveLow(bool activeLow)
{
    SPI_CS_ACTIVELOW = activeLow;
}

void spiEnableGpioCs(bool enable)
{
    SPI_CS_GPIO = enable;
}
