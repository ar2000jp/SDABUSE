/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <sys/time.h>

#ifdef UTILITY_DEBUG
#define DEBUG 1
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "utility.h"

uint32_t getTimeOfDayMsecs(void)
{
    // Doesn't need to cover a long time
    uint32_t retVal;
    struct timeval time;
    gettimeofday(&time, NULL);

    retVal = (time.tv_sec * 1000) + (time.tv_usec / 1000);

    LOG_DEBUG("Time: %u ", retVal);
    return retVal;
}
