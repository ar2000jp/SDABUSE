/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>

#ifdef MAIN_DEBUG
#define DEBUG 1
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "gpio.h"
#include "spi.h"
#include "sd.h"
#include "utility.h"
#include "buse.h"

static struct buse_operations aop;

/* BUSE callbacks */
static int buseSdRead(void* buf, uint32_t len, uint64_t offset, void* userdata)
{
    (void)userdata;

    assert(len >= aop.blksize);

    uint32_t startBlock = offset / aop.blksize;
    uint32_t blockCount = len / aop.blksize;

    LOG_DEBUG("startBlock: %u blockCount: %u", startBlock, blockCount);

    if (!sdReadMultiBlock(startBlock, blockCount, (uint8_t*) buf)) {
        return htonl(EIO);
    }

    return 0;
}

static int buseSdWrite(const void* buf, uint32_t len, uint64_t offset, void* userdata)
{
    (void)userdata;

    assert(len >= aop.blksize);

    uint32_t startBlock = offset / aop.blksize;
    uint32_t blockCount = len / aop.blksize;

    LOG_DEBUG("startBlock: %u blockCount: %u", startBlock, blockCount);

    if (!sdWriteMultiBlock(startBlock, blockCount, (uint8_t*)buf)) {
        return htonl(EIO);
    }

    return 0;
}

static void buseSdDisconnect(void* userdata)
{
    (void)userdata;

    LOG_INFO("Received a disconnect request. Exiting.");

    sdDestroy();
    spiDestroy();
    gpioDestroy();
    fflush(stdout);
}

int main(int argc, char** argv)
{
    LOG_PRINT("SDABUSE: SD As a Block device in USErspace.");
    LOG_PRINT("Copyright 2018 Ahmad Draidi <ar2000jp at gmail dot com>.\n");

    char* nbdDevPath = "/dev/nbd0";
    char* spiDevPath = "/dev/spidev0.1";

    uint8_t spiReadMode = SpiModes.DEV;
    uint8_t spiWriteMode = SpiModes.DEV;

    bool spiGpioCsEnabled = false;
    bool spiCsActiveHigh = false;

    uintptr_t gpioBaseRegAddr = 0x18040000;
    uint8_t gpioModeRegOffset = 0x0;
    uint8_t gpioInputRegOffset = 0x4;
    uint8_t gpioOutputRegOffset = 0x8;

    uint8_t spiGpioCsPin = 0;
    uint8_t spiGpioSckPin = 15;
    uint8_t spiGpioMisoPin = 12;
    uint8_t spiGpioMosiPin = 14;

    bool sdCrcDisabled = false;

    struct option longopts[] = {
        {"nbd", required_argument, NULL, 'n'},
        {"spidev", required_argument, NULL, 's'},

        {"spi-read-mode", required_argument, NULL, 'r'},
        {"spi-write-mode", required_argument, NULL, 'w'},

        {"cs-gpio", no_argument, (int*)& spiGpioCsEnabled, 1},
        {"cs-active-high", no_argument, (int*)& spiCsActiveHigh, 1},

        {"gpio-base-reg-addr" , required_argument, NULL, 'a'},
        {"gpio-mode-reg-offset", required_argument, NULL, 'b'},
        {"gpio-input-reg-offset", required_argument, NULL, 'c'},
        {"gpio-output-reg-offset", required_argument, NULL, 'd'},

        {"gpio-cs-pin", required_argument, NULL, 'i'},
        {"gpio-sck-pin", required_argument, NULL, 'j'},
        {"gpio-miso-pin", required_argument, NULL, 'k'},
        {"gpio-mosi-pin", required_argument, NULL, 'l'},

        {"sd-crc-disable", no_argument, (int*)& sdCrcDisabled, 1},

        {"help", no_argument, NULL, 'h'},

        { 0, 0, 0, 0 }
    };

    int optChar;
    while ((optChar = getopt_long(argc, argv, ":n:s:r:w:a:b:c:d:i:j:k:l:h",
                                  longopts, NULL)) != -1) {
        switch (optChar) {
        case 'n':
            nbdDevPath = optarg;
            break;
        case 's':
            spiDevPath = optarg;
            break;
        case 'r':
            spiReadMode = strtoul(optarg, NULL, 0);
            break;
        case 'w':
            spiWriteMode = strtoul(optarg, NULL, 0);
            break;
        case 'a':
            gpioBaseRegAddr = strtoumax(optarg, NULL, 0);
            break;
        case 'b':
            gpioModeRegOffset = strtoul(optarg, NULL, 0);
            break;
        case 'c':
            gpioInputRegOffset = strtoul(optarg, NULL, 0);
            break;
        case 'd':
            gpioOutputRegOffset = strtoul(optarg, NULL, 0);
            break;
        case 'i':
            spiGpioCsPin = strtoul(optarg, NULL, 0);
            break;
        case 'j':
            spiGpioSckPin = strtoul(optarg, NULL, 0);
            break;
        case 'k':
            spiGpioMisoPin = strtoul(optarg, NULL, 0);
            break;
        case 'l':
            spiGpioMosiPin = strtoul(optarg, NULL, 0);
            break;
        case 'h':
            LOG_PRINT("This program exposes an SD card accessed"
                      " through SPI as an NBD block device.");
            LOG_PRINT(" ");
            LOG_PRINT("Available options are:");
            LOG_PRINT("-h, --help");
            LOG_PRINT("\tShow this help text.");
            LOG_PRINT(" ");
            LOG_PRINT("--nbd device_path");
            LOG_PRINT("\tSet nbd device to use.");
            LOG_PRINT("\tDefault is %s", nbdDevPath);
            LOG_PRINT(" ");
            LOG_PRINT("--spidev device_path");
            LOG_PRINT("\tSet spidev device to use.");
            LOG_PRINT("\tDefault is %s", spiDevPath);
            LOG_PRINT(" ");
            LOG_PRINT("--spi-read-mode mode_number");
            LOG_PRINT("\tSet SPI read mode.");
            LOG_PRINT("\tValid modes are: GPIO (%d), DEV (%d). "
                      "Default is mode %d", SpiModes.GPIO,
                      SpiModes.DEV, spiReadMode);
            LOG_PRINT(" ");
            LOG_PRINT("--spi-write-mode mode_number");
            LOG_PRINT("\tSet SPI write mode.");
            LOG_PRINT("\tValid modes are: GPIO (%d), DEV (%d). "
                      "Default is mode %d", SpiModes.GPIO,
                      SpiModes.DEV, spiWriteMode);
            LOG_PRINT(" ");
            LOG_PRINT("--cs-gpio");
            LOG_PRINT("\tEnable CS line GPIO control.");
            LOG_PRINT("\tDefault is %s",
                      (spiGpioCsEnabled ? "enabled" : "disabled"));
            LOG_PRINT(" ");
            LOG_PRINT("--cs-active-high");
            LOG_PRINT("\tSet CS line to active high.");
            LOG_PRINT("\tDefault is %s",
                      (spiCsActiveHigh ? "enabled" : "disabled"));
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-base-reg-addr address");
            LOG_PRINT("\tSet GPIO control register base address.");
            LOG_PRINT("\tDefault is 0x%"PRIXPTR, gpioBaseRegAddr);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-mode-reg-offset offset");
            LOG_PRINT("\tSet GPIO mode control register offset.");
            LOG_PRINT("\tDefault is 0x%X", gpioModeRegOffset);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-input-reg-offset offset");
            LOG_PRINT("\tSet GPIO input control register offset.");
            LOG_PRINT("\tDefault is 0x%X", gpioInputRegOffset);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-output-reg-offset offset");
            LOG_PRINT("\tSet GPIO output control register offset.");
            LOG_PRINT("\tDefault is 0x%X", gpioOutputRegOffset);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-cs-pin pin_number");
            LOG_PRINT("\tSet GPIO CS pin number.");
            LOG_PRINT("\tDefault is %u", spiGpioCsPin);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-sck-pin pin_number");
            LOG_PRINT("\tSet GPIO SCK pin number.");
            LOG_PRINT("\tDefault is %u", spiGpioSckPin);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-miso-pin pin_number");
            LOG_PRINT("\tSet GPIO MISO pin number.");
            LOG_PRINT("\tDefault is %u", spiGpioMisoPin);
            LOG_PRINT(" ");
            LOG_PRINT("--gpio-mosi-pin pin_number");
            LOG_PRINT("\tSet GPIO CS pin number.");
            LOG_PRINT("\tDefault is %u", spiGpioMosiPin);
            LOG_PRINT(" ");
            LOG_PRINT("--sd-crc-disable");
            LOG_PRINT("\tDisable CRC checking for transfers.");
            LOG_PRINT("\tDefault is %s",
                      (sdCrcDisabled ? "disabled" : "enabled"));
            LOG_PRINT(" ");

            exit(1);
            break;
        case 0:
            break;
        case ':':
        case '?':
        default:
            LOG_FATAL("Invalid arguments.");
            LOG_PRINT("Try \'%s -h\' for more information.", argv[0]);
            exit(1);
            break;
        }
    }

    if (optind != argc) {
        LOG_FATAL("Invalid arguments.");
        LOG_PRINT("Try \'%s -h\' for more information.", argv[0]);
        exit(1);
    }

    if (spiReadMode == SpiModes.GPIO || spiWriteMode == SpiModes.GPIO) {
        gpioInit(gpioBaseRegAddr);
        gpioSetModeRegOffset(gpioModeRegOffset);
        gpioSetInputRegOffset(gpioInputRegOffset);
        gpioSetOutputRegOffset(gpioOutputRegOffset);
    }

    spiSetGpioPins(spiGpioCsPin, spiGpioSckPin, spiGpioMisoPin, spiGpioMosiPin);
    spiSetCsActiveLow(!spiCsActiveHigh);
    spiEnableGpioCs(spiGpioCsEnabled);

    spiSetReadMode(spiReadMode);
    spiSetWriteMode(spiWriteMode);

    if (spiReadMode  == SpiModes.DEV || spiWriteMode == SpiModes.DEV) {
        spiSetSpiDevPath(spiDevPath);
        spiInit();
    }

    sdEnableCrc(!sdCrcDisabled);

    if (!sdInit()) {
        LOG_FATAL("Card init failed.");
        exit(1);
    } else {
        LOG_INFO("Card init OK.");
    }

    char* typeStr;
    uint8_t type = sdGetCardType();
    if (type == SdCardTypes.SD1) {
        typeStr = "SD1";
    } else if (type == SdCardTypes.SD2) {
        typeStr = "SD2";
    } else if (type == SdCardTypes.SDHC) {
        typeStr = "SDHC";
    } else {
        typeStr = "Unknown";
    }
    LOG_INFO("Card type: %s", typeStr);
    if (type == SdCardTypes.SD1 || type == SdCardTypes.UNKNOWN) {
        LOG_FATAL("Bad or unimplemented card type.");
        exit(1);
    }

    uint64_t cardSizeBytes = sdGetCardSize();
    uint16_t cardBlkSize = sdGetCardBlkSize();

    LOG_INFO("Card size: %"PRIu64" MiBs.", cardSizeBytes / (1024 * 1024));
    LOG_INFO("Card block size: %u bytes.", cardBlkSize);

    aop.read = buseSdRead;
    aop.write = buseSdWrite;
    aop.disc = buseSdDisconnect;
    aop.blksize = cardBlkSize;
    aop.size_blocks = cardSizeBytes / cardBlkSize;

    return buse_main(nbdDevPath, &aop, NULL);
}
