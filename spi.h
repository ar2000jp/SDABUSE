/*
 * Copyright 2018,
 * Ahmad Draidi and the SDABUSE contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SPI_H
#define SPI_H

#include <stdint.h>
#include <stdbool.h>

struct _SPI_MODES {
    const uint8_t GPIO;
    const uint8_t DEV;
};
extern const struct _SPI_MODES SpiModes;

void spiInit(void);
void spiDestroy(void);

void spiSetGpioPins(uint8_t csPin, uint8_t sckPin,
                    uint8_t misoPin, uint8_t mosiPin);
void spiSetSpiDevPath(char* devFilePath);

void spiSetReadMode(uint8_t mode);
void spiSetWriteMode(uint8_t mode);

void spiSetCsActiveLow(bool activeLow);
void spiEnableGpioCs(bool enable);

uint16_t spiRead(uint8_t* dataBuf, uint16_t length);
uint16_t spiWrite(const uint8_t* dataBuf, uint16_t length);

uint8_t spiReadByte(void);
void spiWriteByte(uint8_t data);

void spiSelectChip(void);
void spiDeselectChip(void);

#endif // SPI_H
